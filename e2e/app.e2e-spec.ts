import { DataProjectPage } from './app.po';

describe('data-project App', function() {
  let page: DataProjectPage;

  beforeEach(() => {
    page = new DataProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
